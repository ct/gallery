#! /bin/bash

CONDA_HOME=`which conda`
CONDA_HOME=`dirname ${CONDA_HOME}`
CONDA_ENVS=${HOME}/.conda/envs
JUPYTER_DIR=`jupyter --data-dir`

if [ -d ${CONDA_ENVS}/$1 ]
then
   source ${CONDA_HOME}/activate ${CONDA_ENVS}/$1
   python -m ipykernel install --user --name $1
   conda deactivate
   kernda ${JUPYTER_DIR}/kernels/$1/kernel.json -o > /dev/null
else
   echo "Unknown conda environment"
fi
