subroutine curve(alpha, q)
    implicit none
    double precision, intent(in)  :: alpha
    double precision, intent(out) :: q(2)
    
    double precision :: radius
    
    q(1)   = 10d0 * cos(alpha)
    q(2)   = 6d0  * sin(alpha)
    
end subroutine curve