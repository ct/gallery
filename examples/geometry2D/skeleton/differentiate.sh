# hfun
TAPENADE_HOME=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8 \
/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8/bin/tapenade -tangent -fixinterface \
-inputlanguage fortran90 -outputlanguage fortran90 -O ./ -tgtfuncname _d -head "hfun(x, p)>(h)" \
-o hfun \
hfun.f90

# hfun_d
TAPENADE_HOME=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8 \
/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8/bin/tapenade -tangent -fixinterface \
-inputlanguage fortran90 -outputlanguage fortran90 -O ./ -tgtfuncname _d -head "hfun_d(x, p)>(hd)" \
-o hfun_d \
hfun_d.f90

# hfun_d_d
TAPENADE_HOME=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8 \
/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8/bin/tapenade -tangent -fixinterface \
-inputlanguage fortran90 -outputlanguage fortran90 -O ./ -tgtfuncname _d -head "hfun_d_d(x, p)>(hdd)" \
-o hfun_d_d \
hfun_d_d.f90

# curve
TAPENADE_HOME=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8 \
/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8/bin/tapenade -tangent -fixinterface \
-inputlanguage fortran90 -outputlanguage fortran90 -O ./ -tgtfuncname _d -head "curve(alpha)>(q)" \
-o curve \
curve.f90

# point
TAPENADE_HOME=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8 \
/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8/bin/tapenade -tangent -fixinterface \
-inputlanguage fortran90 -outputlanguage fortran90 -O ./ -tgtfuncname _d -head "point(alpha)>(q, p)" \
-o point \
point.f90 curve_d.f90

# point_d
TAPENADE_HOME=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8 \
/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8/bin/tapenade -tangent -fixinterface \
-inputlanguage fortran90 -outputlanguage fortran90 -O ./ -tgtfuncname _d -head "point_d(alpha)>(qd0, pd)" \
-o point_d \
point_d.f90