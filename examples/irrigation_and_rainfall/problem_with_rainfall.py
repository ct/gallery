import bocop
problem_path = "." # using local problem definition

## Building the project
bocop.build(problem_path,cmake_options = '-DCMAKE_CXX_COMPILER=g++')
## Run the program
bocop.run(problem_path,graph=0,verbose=0)