# Imports
import numpy as np
import matplotlib.pyplot as plt
import bocop
import os, shutil


problem_path = "."
bocop.build(problem_path,cmake_options = '-DCMAKE_CXX_COMPILER=g++')
bocop.run(problem_path,graph=0,verbose=1)