# Flow
using LinearAlgebra
using ForwardDiff
using DifferentialEquations
#using ODEInterfaceDiffEq
#using Sundials

grad(f, x) = ForwardDiff.gradient(f, x)

jac(f, x) = ForwardDiff.jacobian(f, x)

function Flow(h)
    """
    f = Flow(h)
    Build the Hamiltonian flow
    
    Input:
    h : Hamiltonian, function
        trueH = h(x,p;par=[])
        Input:
            x : state, Float(n)
            p : co-state, Float(n)
            par : parameters
        Output:
            trueH : value of the Hamiltonian, Float
    Output
    f : flow, function
        sol = f(tspan, z0; λ=[], optionsODE = Dict()))
        Input:
            tspan : tuple of initial and final time (t0, tf)
            z0 : initial point, float(2n)
        Optional input:
            λ : parameters
            optionsODE : dictionary of options for the numerical integration
        Output:
            sol : solution of the (IVP)
       
        sol = f(t0, z0, tf; λ=[], optionsODE = Dict()))
        Output:
            sol : solution of the (IVP) at tf
    """

    function hvfun(z, λ, t)
        """
            Hvec = hvfun(z, λ, t)
            Compute the second membre of the hamiltonan system
            
            Input:
                z : state and co-state [x ; p], Float(2n)
                λ : parameters
                t : time, Float
            Output:
                Hvec : second member , Float(2n)
        """
        n = size(z, 1)÷2
        if λ==[]
            foo = z -> h(z[1:n], z[n+1:2*n])
        else
            foo = z -> h(z[1:n], z[n+1:2*n],λ)
        end
        dh = grad(foo, z)                      # Automatic differentiation

        return [dh[n+1:2n] ; -dh[1:n]]
    end
    
      function f(tspan, z0; λ=[], optionsODE = Dict())
        
        ode = ODEProblem(hvfun, z0, tspan, λ)
        algo = get(optionsODE,:algo,Tsit5())
        RelTol = get(optionsODE,:reltol,1.e-8)
        AbsTol = get(optionsODE,:abstol,1.e-12)
        saveat = get(optionsODE,:saveat,[])
        sol = solve(ode, algo, reltol = RelTol, abstol = AbsTol, saveat = saveat)
        return sol
    end
    
    function f(t0, z0, tf; λ=[], optionsODE = Dict())
        sol = f((t0, tf), z0, λ=λ, optionsODE = optionsODE)
        deux_n = size(z0, 1)
        return sol[1:deux_n, end]
    end
    
    return f

end
