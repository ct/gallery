
// +++DRAFT+++ This class implements the OCP functions
// It derives from the generic class bocop3OCPBase
// OCP functions are defined with templates since they will be called
// from both the NLP solver (double arguments) and AD tool (ad_double arguments)
//#pragma once

#include <OCP.h>

template <typename Variable>
void OCP::finalCost(double initial_time, double final_time, const Variable *initial_state, const Variable *final_state, const Variable *parameters, const double *constants, Variable &final_cost)
{
  	int t;
	final_cost = 0;
	
	double prix  = constants[13];

    for( t = 0; t < 4; t = t + 1 ){
		int q = t*5;

		final_cost = final_cost - final_state[3 + q] + prix*final_state[4 + q];
	}
}

template <typename Variable>
void OCP::dynamics(double time, const Variable *state, const Variable *control, const Variable *parameters, const double *constants, Variable *state_dynamics)
{	
	int t;
    	
	double Lam    = constants[0];
	double ome    = constants[1];
	double gam    = constants[2];
	double nu     = constants[3];
	double mu     = constants[4];
	double muU    = constants[5];
	double d      = constants[6];
	double deltaS = constants[10];
	double deltaI = constants[11];
	double muB    = constants[12];
	double ifcontrol = constants[14];
	
    for( t = 0; t < 4; t = t + 1 ){
		int q = t*5;
		
       	Variable S  = state[0 + q];
		Variable I  = state[1 + q];
		Variable U  = state[2 + q];
		Variable B  = state[3 + q];
		Variable v  = control[t];
		
		Variable lambda = (ome*nu*U)/(S+I);
		
		state_dynamics[0 + q] = Lam - lambda*S - mu*S;
		state_dynamics[1 + q] = lambda*S - (mu+d)*I;
		state_dynamics[2 + q] = gam*I - (nu+muU)*U - v*ifcontrol;
		state_dynamics[3 + q] = deltaS*S + deltaI*I - muB*B;
		state_dynamics[4 + q] = v;
    }

}

template <typename Variable>
void OCP::boundaryConditions(double initial_time, double final_time, const Variable *initial_state, const Variable *final_state, const Variable *parameters, const double *constants, Variable *boundary_conditions)
{
    boundary_conditions[0] = initial_state[0];
    boundary_conditions[1] = initial_state[1];
    boundary_conditions[2] = initial_state[2];
    boundary_conditions[3] = initial_state[3];
    boundary_conditions[4] = initial_state[4];
    
    double phiS   = constants[7];
	double phiI   = constants[8];
	double phiU   = constants[9];
    
    int t;

    for( t = 0; t < 3; t = t + 1 ){
		int q = t*5;

		boundary_conditions[5 + q] = initial_state[5 + q] - final_state[0 + q]*phiS;
		boundary_conditions[6 + q] = initial_state[6 + q] - final_state[1 + q]*phiI;
		boundary_conditions[7 + q] = initial_state[7 + q] - final_state[2 + q]*phiU;
		boundary_conditions[8 + q] = initial_state[8 + q] - final_state[3 + q]*0;
		boundary_conditions[9 + q] = initial_state[9 + q] - final_state[4 + q];
	}
}

template <typename Variable>
void OCP::pathConstraints(double time, const Variable *state, const Variable *control, const Variable *parameters, const double *constants, Variable *path_constraints)
{}

void OCP::preProcessing()
{}

// ///////////////////////////////////////////////////////////////////
// explicit template instanciation for template functions, with double and double_ad 
// +++ could be in an included separate file ? 
// but needs to be done for aux functions too ? APPARENTLY NOT !
template void OCP::finalCost<double>(double initial_time, double final_time, const double *initial_state, const double *final_state, const double *parameters, const double *constants, double &final_cost);
template void OCP::dynamics<double>(double time, const double *state, const double *control, const double *parameters, const double *constants, double *state_dynamics);
template void OCP::boundaryConditions<double>(double initial_time, double final_time, const double *initial_state, const double *final_state, const double *parameters, const double *constants, double *boundary_conditions);
template void OCP::pathConstraints<double>(double time, const double *state, const double *control, const double *parameters, const double *constants, double *path_constraints);

template void OCP::finalCost<double_ad>(double initial_time, double final_time, const double_ad *initial_state, const double_ad *final_state, const double_ad *parameters, const double *constants, double_ad &final_cost);
template void OCP::dynamics<double_ad>(double time, const double_ad *state, const double_ad *control, const double_ad *parameters, const double *constants, double_ad *state_dynamics);
template void OCP::boundaryConditions<double_ad>(double initial_time, double final_time, const double_ad *initial_state, const double_ad *final_state, const double_ad *parameters, const double *constants, double_ad *boundary_conditions);
template void OCP::pathConstraints<double_ad>(double time, const double_ad *state, const double_ad *control, const double_ad *parameters, const double *constants, double_ad *path_constraints);
