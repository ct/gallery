:orphan:

=========
Notebooks
=========

Agronomics
==========

.. nbgallery::

    clr/clr
    irrigation/irrigation
    irrigation_and_rainfall/irrigation_and_rainfall

Biology
=======

.. nbgallery::

    bistable/bistable
    bacteria/bacteria
    stability/stability
    substrate/depletion

Direct methods
==============

.. nbgallery::

    goddard/goddard
    goddard-j/goddard
    goddard-j/goddard_InfOpt
    nlp_indirect/nlp1
    nlp_indirect/nlp2


Indirect methods (shooting and homotopy)
========================================

.. nbgallery::

    shooting_tutorials/simple_shooting_general
    shooting_tutorials/multiple_shooting_homotopy
    irrigation_hybrid/hybrid
    callback-simple/singular1d
    homotopy-julia/FGS

Indirect and direct methods
===========================

.. nbgallery::

    smooth_case/smooth_case
    regulator/regulator
    control-loss/double-integrator-with-control-loss-and-no-control

Geometry and navigation problems
================================

.. nbgallery::

    nav/nav-julia-gen
    geometry2D/ellipsoid/ellipsoid
    geometry2D/skeleton/skeleton

Space
=====

.. nbgallery::

    kepler/kepler
    kepler-c/kepler
    kepler-numba/kepler
    kepler-j/kepler
    solarsail/solarsail-simple-version
    solarsail/solarsail-simple-version-implicit

Misc
====

.. nbgallery::

    ihm/ihm
    sir/SIR

Turnpike and singular perturbations
===================================

.. nbgallery::

    turnpike/1d/bsb_turnpike
    turnpike/2d/turnpike
    homotopy-julia/FGS

