#Computational materion for Stability analysis of a bacterial growth model\\through computer algebra, Yabo, Safey El Din, Caillau, Gouz�

SmallMidPoint:=proc(a,b)
local d, n, delta, r, i, it;
if a =b then return a; fi;
if b<a then error "bad interval for constructing rationals"; fi;
it:=2:
while true do 
r:=convert((a+b)/2,confrac,'m', it);
for i from 1 to nops(m) do
   if m[i]> a and m[i]<b then
      return m[i];
   fi;
od:
it:=2*it:
od:
end;

ConstructSmallRational:=proc(a,b)
local d, n, delta;
if a =b then return a; fi;
if b<a then error "bad interval for construction rationals"; fi;
return SmallMidPoint(a, b);
end;
find_rationals:=proc(listrat)
local rationals,expo,apps,boo,den,newapps,i, newrats:
 if nops(listrat)=0 then return [0] fi;
 newrats:=[];
 for i from 2 to nops(listrat) do 
    newrats:= [op(newrats),ConstructSmallRational(listrat[i-1], listrat[i])];
 od;
 newrats:=[floor(listrat[1]-1),op(newrats), ceil(listrat[nops(listrat)]+1)];
return newrats;
end:


## Original problem

## Equilibrium conditions (i.e. dsdt = dVdt = dpdt = 0)
dsdt = V*k2*s*(u - 1)/(K2 + s) - d*(s - 1);
dVdt = -d + p*u/(K + p);
dpdt = -d*(p + 1) + k1*p*(u - 1)/(K1 + p) - k2*s*(u - 1)/(K2 + s);

## Positivity of the variables / parameters (i.e. ineq1 > 0, ineq2 > 0)
inequalities1 = [k1, K1, u, 1-u, k2, K2, d, K];
inequalities2 = [s,V,p];

## Positivity of the inverse Routh-Hurwitz condition (i.e. pol > 0)
pol := V*k2*s*(k2*(u - 1)/(K2 + s) - k2*s*(u - 1)/(K2 + s)^2)*(u - 1)*(u/(K + p) - p*u/(K + p)^2)/(K2 + s) + (V*k2*(u - 1)/(K2 + s) - V*k2*s*(u - 1)/(K2 + s)^2 - d + k1*(u - 1)/(K1 + p) - k1*p*(u - 1)/(K1 + p)^2 - (p + 1)*u/(K + p) - p*u/(K + p) + (p + 1)*p*u/(K + p)^2)*(V*k2*(u - 1)/(K2 + s) - V*k2*s*(u - 1)/(K2 + s)^2 - d)*(k1*(u - 1)/(K1 + p) - k1*p*(u - 1)/(K1 + p)^2 - (p + 1)*u/(K + p) - p*u/(K + p) + (p + 1)*p*u/(K + p)^2);

## The latter means that, if pol>0, the equilibrium is unstable (we want to proove this never occurs, i.e. the problem has no solution).

## Re-parametrized problem
## We replaced K1 = K1p - p and K2 = K2p - s and we replaced the equilibrium conditions in every vector

## Positivity of the variables / parameters (i.e. newineq1 > 0, newineq2 > 0)
newineq1 := [-K2p*d*p - k2*s*u - K2p*d + k2*s, K1p - p, u, -u + 1, k2, K2p - s, d, -d + u];
newineq2 := [s, -s + 1, p]:

## Positivity of the (numerator of the) inverse Routh-Hurwitz condition (i.e. numpol > 0)
# (The latter because we already saw the denominator is always positive)
numpol := -K1p^2*K2p^2*d^4*p^2*s^3 + 2*K1p^2*K2p^2*d^3*p^2*s^3*u + 2*K1p*K2p^2*d^3*p^3*s^3*u + K1p^2*K2p*d^3*p^2*s^4*u - K1p^2*K2p^2*d^2*k2*p*s^3*u^2 - K1p^2*K2p^2*d^2*p^2*s^3*u^2 - 2*K1p*K2p^2*d^2*p^3*s^3*u^2 - K2p^2*d^2*p^4*s^3*u^2 - K1p^2*K2p*d^2*k2*p*s^4*u^2 - K1p^2*K2p*d^2*p^2*s^4*u^2 + 2*K1p*K2p*d^2*k2*p^2*s^4*u^2 - K1p*K2p*d^2*p^3*s^4*u^2 + K1p^2*K2p^2*d*k2*p*s^3*u^3 + K1p^2*K2p*d*k2*p*s^4*u^3 - 2*K2p*d*k2*p^3*s^4*u^3 + K1p^2*d*k2*p*s^5*u^3 - K1p*d*k2*p^2*s^5*u^3 - K1p^2*k2^2*s^5*u^4 + 2*K1p*k2^2*p*s^5*u^4 - k2^2*p^2*s^5*u^4 - K1p^2*K2p^3*d^4*p^2*s + K1p^2*K2p^2*d^4*p^2*s^2 - 2*K1p^2*K2p^2*d^4*p*s^3 + 2*K1p^2*K2p^3*d^3*p^2*s*u + 2*K1p*K2p^3*d^3*p^3*s*u - 2*K1p*K2p^2*d^3*p^3*s^2*u + 2*K1p^2*K2p^2*d^3*p*s^3*u + K1p^2*K2p^2*d^2*k2*p*s^3*u - 2*K1p^2*K2p*d^3*p^2*s^3*u + 4*K1p*K2p^2*d^3*p^2*s^3*u + K1p^2*K2p*d^3*p*s^4*u + K1p^2*K2p*d^2*k2*p*s^4*u - 2*K1p*K2p*d^2*k2*p^2*s^4*u - K1p^2*K2p^3*d^2*p^2*s*u^2 - 2*K1p*K2p^3*d^2*p^3*s*u^2 - K2p^3*d^2*p^4*s*u^2 - K1p^2*K2p^2*d^2*k2*p*s^2*u^2 - K1p^2*K2p^2*d^2*p^2*s^2*u^2 + 2*K1p*K2p^2*d^2*k2*p^2*s^2*u^2 + K2p^2*d^2*p^4*s^2*u^2 - K1p^2*K2p^2*d*k2*p*s^3*u^2 + K1p^2*K2p*d^2*k2*p*s^3*u^2 + 2*K1p^2*K2p*d^2*p^2*s^3*u^2 - 2*K1p*K2p^2*d^2*p^2*s^3*u^2 - 2*K1p*K2p*d^2*k2*p^2*s^3*u^2 + 2*K1p*K2p*d^2*p^3*s^3*u^2 - 2*K2p^2*d^2*p^3*s^3*u^2 - 2*K1p^2*K2p*d^2*k2*s^4*u^2 - K1p^2*K2p*d*k2*p*s^4*u^2 + 2*K1p*K2p*d^2*k2*p*s^4*u^2 - K1p*K2p*d^2*p^2*s^4*u^2 + 2*K2p*d*k2*p^3*s^4*u^2 - K1p^2*d*k2*p*s^5*u^2 + K1p*d*k2*p^2*s^5*u^2 + K1p^2*K2p^2*d*k2*p*s^2*u^3 - 2*K2p^2*d*k2*p^3*s^2*u^3 + K1p^2*K2p*d*k2*p*s^3*u^3 - 2*K1p*K2p*d*k2*p^2*s^3*u^3 + 2*K2p*d*k2*p^3*s^3*u^3 - 2*K1p^2*d*k2*p*s^4*u^3 + 2*K1p*K2p*d*k2*p*s^4*u^3 + 2*K1p*d*k2*p^2*s^4*u^3 - 2*K2p*d*k2*p^2*s^4*u^3 + 2*K1p^2*k2^2*s^5*u^3 - 4*K1p*k2^2*p*s^5*u^3 + 2*k2^2*p^2*s^5*u^3 - K1p^2*K2p*k2^2*s^3*u^4 + 2*K1p*K2p*k2^2*p*s^3*u^4 - K2p*k2^2*p^2*s^3*u^4 + K1p^2*k2^2*s^4*u^4 - 2*K1p*k2^2*p*s^4*u^4 + k2^2*p^2*s^4*u^4 - 2*K1p^2*K2p^3*d^4*p*s + 2*K1p^2*K2p^2*d^4*p*s^2 - K1p^2*K2p^2*d^4*s^3 + K1p^2*K2p^3*d^3*p^2*u + 2*K1p^2*K2p^3*d^3*p*s*u - 2*K1p^2*K2p^2*d^3*p^2*s*u + 4*K1p*K2p^3*d^3*p^2*s*u + K1p^2*K2p^2*d^2*k2*p*s^2*u + K1p^2*K2p*d^3*p^2*s^2*u - 4*K1p*K2p^2*d^3*p^2*s^2*u - 2*K1p*K2p^2*d^2*k2*p^2*s^2*u - 2*K1p^2*K2p*d^3*p*s^3*u + 2*K1p*K2p^2*d^3*p*s^3*u - K1p^2*K2p*d^2*k2*p*s^3*u + 2*K1p*K2p*d^2*k2*p^2*s^3*u + 2*K1p^2*K2p*d^2*k2*s^4*u - 2*K1p*K2p*d^2*k2*p*s^4*u - K1p^2*K2p^3*d^2*p^2*u^2 - K1p*K2p^3*d^2*p^3*u^2 + 2*K1p^2*K2p^2*d^2*p^2*s*u^2 - 2*K1p*K2p^3*d^2*p^2*s*u^2 + 2*K1p*K2p^2*d^2*p^3*s*u^2 - 2*K2p^3*d^2*p^3*s*u^2 - 2*K1p^2*K2p^2*d^2*k2*s^2*u^2 - K1p^2*K2p^2*d*k2*p*s^2*u^2 + 2*K1p*K2p^2*d^2*k2*p*s^2*u^2 - K1p^2*K2p*d^2*p^2*s^2*u^2 - K1p*K2p*d^2*p^3*s^2*u^2 + 2*K2p^2*d^2*p^3*s^2*u^2 + 2*K2p^2*d*k2*p^3*s^2*u^2 + 2*K1p^2*K2p*d^2*k2*s^3*u^2 - K1p^2*K2p*d*k2*p*s^3*u^2 - 2*K1p*K2p*d^2*k2*p*s^3*u^2 + 2*K1p*K2p*d^2*p^2*s^3*u^2 - K2p^2*d^2*p^2*s^3*u^2 + 2*K1p*K2p*d*k2*p^2*s^3*u^2 - 2*K2p*d*k2*p^3*s^3*u^2 + 2*K1p^2*d*k2*p*s^4*u^2 - 2*K1p*K2p*d*k2*p*s^4*u^2 - 2*K1p*d*k2*p^2*s^4*u^2 + 2*K2p*d*k2*p^2*s^4*u^2 - K1p^2*k2^2*s^5*u^2 + 2*K1p*k2^2*p*s^5*u^2 - k2^2*p^2*s^5*u^2 + K1p^2*K2p^2*d*k2*p*s*u^3 - K1p*K2p^2*d*k2*p^2*s*u^3 - 2*K1p^2*K2p*d*k2*p*s^2*u^3 + 2*K1p*K2p^2*d*k2*p*s^2*u^3 + 2*K1p*K2p*d*k2*p^2*s^2*u^3 - 2*K2p^2*d*k2*p^2*s^2*u^3 + 2*K1p^2*K2p*k2^2*s^3*u^3 + K1p^2*d*k2*p*s^3*u^3 - 2*K1p*K2p*d*k2*p*s^3*u^3 - 4*K1p*K2p*k2^2*p*s^3*u^3 - K1p*d*k2*p^2*s^3*u^3 + 2*K2p*d*k2*p^2*s^3*u^3 + 2*K2p*k2^2*p^2*s^3*u^3 - 2*K1p^2*k2^2*s^4*u^3 + 4*K1p*k2^2*p*s^4*u^3 - 2*k2^2*p^2*s^4*u^3 - K1p^2*K2p^3*d^4*s + K1p^2*K2p^2*d^4*s^2 + K1p^2*K2p^3*d^3*p*u - 2*K1p^2*K2p^2*d^3*p*s*u + 2*K1p*K2p^3*d^3*p*s*u + 2*K1p^2*K2p^2*d^2*k2*s^2*u + K1p^2*K2p*d^3*p*s^2*u - 2*K1p*K2p^2*d^3*p*s^2*u - 2*K1p*K2p^2*d^2*k2*p*s^2*u - 2*K1p^2*K2p*d^2*k2*s^3*u + 2*K1p*K2p*d^2*k2*p*s^3*u - K1p*K2p^3*d^2*p^2*u^2 - K1p^2*K2p^2*d*k2*p*s*u^2 + 2*K1p*K2p^2*d^2*p^2*s*u^2 - K2p^3*d^2*p^2*s*u^2 + K1p*K2p^2*d*k2*p^2*s*u^2 + 2*K1p^2*K2p*d*k2*p*s^2*u^2 - 2*K1p*K2p^2*d*k2*p*s^2*u^2 - K1p*K2p*d^2*p^2*s^2*u^2 + K2p^2*d^2*p^2*s^2*u^2 - 2*K1p*K2p*d*k2*p^2*s^2*u^2 + 2*K2p^2*d*k2*p^2*s^2*u^2 - K1p^2*K2p*k2^2*s^3*u^2 - K1p^2*d*k2*p*s^3*u^2 + 2*K1p*K2p*d*k2*p*s^3*u^2 + 2*K1p*K2p*k2^2*p*s^3*u^2 + K1p*d*k2*p^2*s^3*u^2 - 2*K2p*d*k2*p^2*s^3*u^2 - K2p*k2^2*p^2*s^3*u^2 + K1p^2*k2^2*s^4*u^2 - 2*K1p*k2^2*p*s^4*u^2 + k2^2*p^2*s^4*u^2;

ineqs := [op(newineq1), op(newineq2), numpol]:

infolevel[Groebner:-Basis]:=0:
## We expect the semi-algebraic set defined by map(_p->_p > 0, ineqs) to be empty
##This computations looks hard.
##sols:=RAG[HasRealSolutions](map(_p->_p > 0, ineqs)):

##numpol has degree 2 in K1p ; then one eliminates K1p like in a weak CAD.
## We are lucky: these polynomials can be factored. 
res:=factor(resultant(numpol, newineq1[2], K1p))/(K2p*d*p^2);
disc:=factor(discrim(numpol, K1p))/((s^2+K2p-s)*(K2p*d*p+k2*s*u+K2p*d-k2*s)^2*d*p^3*u^3):
##One can get rid of factors which we know are sign invariant. 
##because of the other inequalities.
newineqs:=map(_p->if not(member(K1p, indets(_p))) then _p fi, [op(newineq1), op(newineq2)]):

##res, disc and polynomials in newineqs have degree 1 in k2.
##Then, we also eliminate it. 

##Those polynomials must be positive (we omit k2>0)
newineqs2:=[u, 1 - u, K2p - s, d, -d + u, s, -s + 1, p];
##It remains to deal with  newineqs[1] which has degree 1 in k2.
##Its leading coeff dominant is -s*(u-1) and is sign invariant.
##Its constant coeff has degree 0 and is -K2p*d*(p+1)
##p+1 is >0 (because p>0), s also and K2p also. 
##hence no constraint coming from  newineqs[1].

##It remains  res and disc. We start with res.

##leading coeff is -p*s^2*u*(u-1)*(s-1)*(d-u)*(K2p-s)
##All its factors are sign invariant modulo the above inequalities.
##Constant term is more involved and brings new constraints.
qres:=map(_p->_p[1], factors(factor(coeff(res, k2, 0))/(d*(s^2+K2p-s)))[2]);
##En travaillant un peu on se rend compte facilement que
##d*p-2*p*u+d-u est negatif modulo newineqs2

qres:={op(qres)} minus {d*p-2*p*u+d-u};
qres:=[op(qres)];

##We study now disc as above.
##leading coeff -4*K2p*s^3*(u-1)*(s-1)*(d-u)*(K2p-s)
#is sign invariant modulo newineqs2
#same for contant termwhich is d*p*u*(s^2+K2p-s)^3

#Finally it remains to study the resultant de res and disc (eliminanting k2).
#factor(resultant(res, disc, k2));
#is
#-d*s^2*(u-1)*(s-1)*(d-u)*(s^2+K2p-s)*(K2p-s)*(2*K2p*d*p*s-4*K2p*p*s*u-p*s^2*u+2*K2p*d*s-K2p*p*u-2*K2p*s*u+p*s*u)^2
#sign invariant modulo newineqs2.

#It then remains to solve
sols:=RAG[PointsPerComponents]([op(map(_p->_p>0, newineqs2)), op(map(_p->_p<>0, qres))]):
lprint(sols);

#One finds for instance
sols:=[[K2p = 1, d = 149521988969/274877906944, p = 126991278303/274877906944, s = 170417339309/274877906944, u = 149934875065/274877906944]];

newsols:=[];
newsols2:=[];
for i from 1 to nops(sols) do
pol:=primpart(mul(_p, _p in subs(sols[i], [op(newineqs), k2, res, disc])));
rroots:=map(rhs, RootFinding[Isolate](pol, output=midpoint));
rats:=find_rationals(rroots);
newsols:=[op(newsols), op(map(r->[k2=r, op(sols[i])], rats))];
  for j from 1 to nops(newsols) do
    pol:=primpart(mul(_p, _p in subs(newsols[j], ineqs)));
    rroots:=map(rhs, RootFinding[Isolate](pol, output=midpoint));
    rats:=find_rationals(rroots);
    newsols2:=[op(newsols2), op(map(r->[K1p=r, op(newsols[j])], rats))];
  od:
od;
finalsols:=[];
for i from  1 to nops(newsols2) do
if not(member(-1, map(sign, subs(newsols2[i], ineqs)))) then
  finalsols:=[op(finalsols), newsols2[i]];
fi;
od;
#quit;
