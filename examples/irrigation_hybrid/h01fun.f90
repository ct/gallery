Subroutine h01fun(t,n,x,p,npar,par,h01)
    implicit none
    integer, intent(in)                             :: n,npar
    double precision, intent(in)                    :: t
    double precision, dimension(n),    intent(in)   :: x, p
    double precision, dimension(npar), intent(in)   :: par
    double precision,                  intent(out)  :: h01

    !Local variables
    double precision                                :: xd(n), pd(n), h0, h1, h0d, h1d
    integer                                         :: i

    h01     = 0d0
    xd      = 0d0
    pd      = 0d0
    do i = 1, n

        pd(i) = 1d0
        call H1FUN_DC1(n, p, pd, npar, par, h1, h1d)
        pd(i) = 0d0

        xd(i) = 1d0
        call H0FUN_DC0(t, n, x, xd, p, pd, npar, par, h0, h0d)
        xd(i) = 0d0

        h01 = h01 - h0d*h1d

    end do

end subroutine h01fun
