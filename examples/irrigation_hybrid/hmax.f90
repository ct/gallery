Subroutine hmax(t, n, x, p, npar, par, H)
    implicit none
    integer,                           intent(in)  :: n, npar
    double precision,                  intent(in)  :: t
    double precision, dimension(n),    intent(in)  :: x, p
    double precision, dimension(npar), intent(in)  :: par
    double precision,                  intent(out) :: H
    
    ! Local declarations
    double precision    :: H0, H1
    call h0fun(t, n, x, p, npar, par, H0)
    call h1fun(n, p, npar, par, H1)
    H = H0 + H1

end subroutine hmax
