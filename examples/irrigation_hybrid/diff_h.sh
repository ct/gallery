# Need to add INTENT(OUT) after differentiation

TAP=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8
#OPTT='-tangent -fixinterface -inputlanguage fortran90 -outputlanguage fortran90 -nooptim activity'
OPTT='-tangent -fixinterface -inputlanguage fortran90 -outputlanguage fortran90'

# --------------------------------------------------------
# hmin -> hmin_d
TAPENADE_HOME=${TAP} ${TAP}/bin/tapenade ${OPTT} -O ./ -tgtfuncname _dmi -head "hmin(x, p, par)>(h)" \
-o hmin hmin.f90 h0fun.f90 parameters.f90

# hmin_d -> hmin_d_d
TAPENADE_HOME=${TAP} ${TAP}/bin/tapenade ${OPTT} -O ./ -tgtfuncname _dmi -head "hmin_dmi(x, p, par)>(hd)" \
-o hmin_dmi hmin_dmi.f90

# --------------------------------------------------------
# hmax -> hmax_d
TAPENADE_HOME=${TAP} ${TAP}/bin/tapenade ${OPTT} -O ./ -tgtfuncname _dma -head "hmax(x, p, par)>(h)" \
-o hmax hmax.f90 h0fun.f90 h1fun.f90 parameters.f90

# hmax_d -> hmax_d_d
TAPENADE_HOME=${TAP} ${TAP}/bin/tapenade ${OPTT} -O ./ -tgtfuncname _dma -head "hmax_dma(x, p, par)>(hd)" \
-o hmax_dma hmax_dma.f90  parameters.f90

# --------------------------------------------------------
# hsing -> hsing_d
TAPENADE_HOME=${TAP} ${TAP}/bin/tapenade ${OPTT} -O ./ -tgtfuncname _dus -head "hsing(x, p, par)>(h)" \
-o hsing hsing.f90 h0fun.f90 h1fun.f90 parameters.f90 singularcontrol.f90 h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90

# hsing_d -> hsing_d_d
TAPENADE_HOME=${TAP} ${TAP}/bin/tapenade ${OPTT} -O ./ -tgtfuncname _dus -head "hsing_dus(x, p, par)>(hd)" \
-o hsing_dus hsing_dus.f90 h1fun_dc1.f90 parameters.f90