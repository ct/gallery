Subroutine hmin(t, n, x, p, npar, par, H)
    implicit none
    integer,                           intent(in)  :: n, npar
    double precision,                  intent(in)  :: t
    double precision, dimension(n),    intent(in)  :: x, p
    double precision, dimension(npar), intent(in)  :: par
    double precision,                  intent(out) :: H
    
    ! Local declarations
    double precision    :: H0
    call h0fun(t, n, x, p, npar, par, H0)
    H = H0

end subroutine hmin