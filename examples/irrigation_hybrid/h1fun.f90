Subroutine h1fun(n,p,npar,par,h1)
    implicit none
    integer, intent(in)                             :: n,npar
    double precision, dimension(n),    intent(in)   :: p
    double precision, dimension(npar), intent(in)   :: par
    double precision, intent(out)                   :: h1

    !Local declarations
    double precision    :: Tf, k1, k2, Ss, Sw, Sh, Fmax, Qbar, alpha, r, Bmax, B0, eta, pzero

    ! Parameters
    call parameters(npar, par, Tf, k1, k2, Ss, Sw, Sh, Fmax, Qbar, alpha, r, Bmax, B0, eta, pzero)

    ! H1
    h1 = p(1)*k1*k2 + p(3)

end subroutine h1fun