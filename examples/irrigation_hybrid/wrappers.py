import os
import nutopy as nt
import numpy as np 

def singular_control(compile=False, display=False):
    
    if display:
        out=''
    else:
        out='> /dev/null 2>&1'

    if not compile:
        try:
            from singularcontrol import singularcontrol as sc
        except ModuleNotFoundError:
            compile = True
    
    if compile:
        # Compiling singular control
        os.system('python -m numpy.f2py -c singularcontrol.f90 \
        h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 parameters.f90 \
        -m singularcontrol ' + out)
        from singularcontrol import singularcontrol as sc
    
    using_split = lambda t, x, p, eta : sc(t, x, p, np.array([eta]))

    @nt.tools.vectorize(vvars=(1, 2, 3))
    def using(t, x, p, eta):
        num, den = using_split(t, x, p, eta)
        return -num/den

    return using

def hamiltonian_umin(compile=False, display=False):
    
    if display:
        out=''
    else:
        out='> /dev/null 2>&1'
    
    if not compile:
        try:
            from hmin          import hmin         as hf_min
            from hmin_dmi      import hmin_dmi     as hf_min_d
            from hmin_dmi_dmi  import hmin_dmi_dmi as hf_min_d_d
        except ModuleNotFoundError:
            compile = True
        
    if compile:
        # Compiling hmin
        os.system('python -m numpy.f2py -c hmin.f90 h0fun.f90 parameters.f90 -m hmin ' + out)
        os.system('python -m numpy.f2py -c hmin_dmi.f90     -m hmin_dmi     ' + out)
        os.system('python -m numpy.f2py -c hmin_dmi_dmi.f90 -m hmin_dmi_dmi ' + out)
        from hmin          import hmin         as hf_min
        from hmin_dmi      import hmin_dmi     as hf_min_d
        from hmin_dmi_dmi  import hmin_dmi_dmi as hf_min_d_d

    # Hamiltonian and flow
    hfun_min   = lambda t, x, p, e                : hf_min(t, x, p, np.array([e]))
    dhfun_min  = lambda t, x, dx, p, dp, e, de    : hf_min_d(t, x, dx, p, dp, np.array([e]), np.array([de]))
    d2hfun_min = lambda t, x, dx, d2x, p, dp, d2p, e, de, d2e : hf_min_d_d(t, x, d2x, dx, p, d2p, dp, \
                                                                           np.array([e]), np.array([d2e]), np.array([de]))

    hfun_min   = nt.tools.tensorize(dhfun_min, d2hfun_min, tvars=(2, 3, 4), full=True)(hfun_min)
    hmin       = nt.ocp.Hamiltonian(hfun_min)
    
    return hmin

def hamiltonian_umax(compile=False, display=False):
    
    if display:
        out=''
    else:
        out='> /dev/null 2>&1'

    if not compile:
        try:
            from hmax         import hmax         as hf_max
            from hmax_dma     import hmax_dma     as hf_max_d
            from hmax_dma_dma import hmax_dma_dma as hf_max_d_d
        except ModuleNotFoundError:
            compile = True
            
    if compile:
        # Compiling hmax
        os.system('python -m numpy.f2py -c hmax.f90 h0fun.f90 h1fun.f90 parameters.f90 -m hmax ' + out)
        os.system('python -m numpy.f2py -c hmax_dma.f90 parameters.f90 -m hmax_dma     ' + out)
        os.system('python -m numpy.f2py -c hmax_dma_dma.f90 hmax_dma.f90 parameters.f90 -m hmax_dma_dma ' + out)
        from hmax         import hmax         as hf_max
        from hmax_dma     import hmax_dma     as hf_max_d
        from hmax_dma_dma import hmax_dma_dma as hf_max_d_d

    # Hamiltonian and flow
    hfun_max   = lambda t, x, p, e                : hf_max(t, x, p, np.array([e]))
    dhfun_max  = lambda t, x, dx, p, dp, e, de    : hf_max_d(t, x, dx, p, dp, np.array([e]), np.array([de]))
    d2hfun_max = lambda t, x, dx, d2x, p, dp, d2p, e, de, d2e : hf_max_d_d(t, x, d2x, dx, p, d2p, dp, \
                                                                           np.array([e]), np.array([d2e]), np.array([de]))

    hfun_max   = nt.tools.tensorize(dhfun_max, d2hfun_max, tvars=(2, 3, 4), full=True)(hfun_max)
    hmax       = nt.ocp.Hamiltonian(hfun_max)
    
    return hmax

def hamiltonian_using(compile=False, display=False):
    
    if display:
        out=''
    else:
        out='> /dev/null 2>&1'

    if not compile:
        try:
            from hsing         import hsing         as hf_sing
            from hsing_dus     import hsing_dus     as hf_sing_d
            from hsing_dus_dus import hsing_dus_dus as hf_sing_d_d
        except ModuleNotFoundError:
            compile = True
            
    if compile:
        # Compiling hsing
        os.system('python -m numpy.f2py -c hsing.f90 h0fun.f90 h1fun.f90 parameters.f90 singularcontrol.f90 \
        h01fun_d01.f90 h0fun_dc0.f90 h1fun_dc1.f90 -m hsing ' + out)
        os.system('python -m numpy.f2py -c hsing_dus.f90 parameters.f90 h1fun_dc1.f90 -m hsing_dus     ' + out)
        os.system('python -m numpy.f2py -c hsing_dus_dus.f90 hsing_dus.f90 parameters.f90 h1fun_dc1.f90 -m hsing_dus_dus ' + out)
        from hsing         import hsing         as hf_sing
        from hsing_dus     import hsing_dus     as hf_sing_d
        from hsing_dus_dus import hsing_dus_dus as hf_sing_d_d

    # Hamiltonian and flow
    hfun_sing   = lambda t, x, p, e                : hf_sing(t, x, p, np.array([e]))
    dhfun_sing  = lambda t, x, dx, p, dp, e, de    : hf_sing_d(t, x, dx, p, dp, np.array([e]), np.array([de]))
    d2hfun_sing = lambda t, x, dx, d2x, p, dp, d2p, e, de, d2e : hf_sing_d_d(t, x, d2x, dx, p, d2p, dp, \
                                                                             np.array([e]), np.array([d2e]), np.array([de]))

    hfun_sing   = nt.tools.tensorize(dhfun_sing, d2hfun_sing, tvars=(2, 3, 4), full=True)(hfun_sing)
    hsing       = nt.ocp.Hamiltonian(hfun_sing)
    
    return hsing

def switching_function(compile=False, display=False):
    
    if display:
        out=''
    else:
        out='> /dev/null 2>&1'
    
    if not compile:
        try:
            from h1fun         import h1fun         as h1f
            from h1fun_dc1     import h1fun_dc1     as h1f_d
        except ModuleNotFoundError:
            compile = True
            
    if compile:
        # Compiling H1
        os.system('python -m numpy.f2py -c h1fun.f90 parameters.f90 -m h1fun     ' + out)
        os.system('python -m numpy.f2py -c h1fun_dc1.f90 parameters.f90 -m h1fun_dc1 ' + out)
        from h1fun         import h1fun         as h1f
        from h1fun_dc1     import h1fun_dc1     as h1f_d

    h1fun   = lambda p     : h1f(p, np.array([0.0]))
    dh1fun  = lambda p, dp : h1f_d(p, dp, np.array([0.0]))
    h1fun   = nt.tools.tensorize(dh1fun, tvars=(1, ), full=True)(h1fun)
    h1fun   = nt.tools.vectorize(vvars=(1, ))(h1fun)
    #h1      = nt.ocp.Hamiltonian(h1fun)
    h1 = h1fun
    
    return h1

def lift_H01(compile=False, display=False):
    
    if display:
        out=''
    else:
        out='> /dev/null 2>&1'
        
    if not compile:
        try:
            from h01fun         import h01fun         as h01f
            from h01fun_d01     import h01fun_d01     as h01f_d
        except ModuleNotFoundError:
            compile = True
            
    if compile:
        # Compiling H1
        os.system('python -m numpy.f2py -c h01fun.f90 h0fun_dc0.f90 h1fun_dc1.f90 parameters.f90 -m h01fun     ' + out)
        os.system('python -m numpy.f2py -c h01fun_d01.f90 h1fun_dc1.f90 parameters.f90 -m h01fun_d01 ' + out)
        from h01fun         import h01fun         as h01f
        from h01fun_d01     import h01fun_d01     as h01f_d

    h01fun   = lambda t, x, p, e             : h01f(t, x, p, np.array([e]))
    dh01fun  = lambda t, dt, x, dx, p, dp, e, de : h01f_d(t, dt, x, dx, p, dp, np.array([e]), np.array([de]))
    h01fun   = nt.tools.tensorize(dh01fun, tvars=(1, 2, 3, 4), full=True)(h01fun)
    #h01      = nt.ocp.Hamiltonian(h01fun)
    h01 = h01fun
    
    return h01
