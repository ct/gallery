Subroutine h0fun(t,n,x,p,npar,par,h0)
    implicit none
    integer, intent(in)                             :: n, npar
    double precision, intent(in)                    :: t
    double precision, dimension(n),    intent(in)   :: x, p
    double precision, dimension(npar), intent(in)   :: par
    double precision, intent(out)                   :: h0

    !Local declarations
    double precision    :: KS, KR, phi, f
    double precision    :: S, B, theta, Sc, Scmoins
    double precision    :: Tf, k1, k2, Ss, Sw, Sh, Fmax, Qbar, alpha, r, Bmax, B0, eta, pzero

    ! Parameters
    call parameters(npar, par, Tf, k1, k2, Ss, Sw, Sh, Fmax, Qbar, alpha, r, Bmax, B0, eta, pzero)

    ! Variables
    S = x(1)
    B = x(2)
    
    ! KS Regularization
    theta   = atan(Ss-Sw)
    Sc      = Sw+(Ss-Sw)*(1d0-eta+eta*sin(theta))+eta*cos(theta)
    Scmoins = Sc-eta*cos(theta)
    if ((S>Scmoins) .and. (S<Sc)) then
        KS = 1d0-eta+sqrt(eta**2-(S-Sc)**2)
    else
        if(S.le.Sw)then
            KS = 0d0
        elseif(S.ge.Ss)then
            KS = 1d0
        else
            KS = (S-Sw)/(Ss-Sw)
        end if
    endif

    ! KR
    if(S.le.Sh)then
        KR = 0d0
    elseif(S.ge.1d0)then
        KR = 1d0
    else
        KR = (S-Sh)/(1d0-Sh)
    end if

    ! phi and f
    phi = (t/Tf)**alpha
!    f   = 1d0 !r*B*(1d0-B/Bmax)
    f   = r*B*(1d0-B/Bmax)
    
    ! H0
    h0 = -p(1)*k1*(phi*KS+(1d0-phi)*KR) + p(2)*phi*KS*f

end subroutine h0fun