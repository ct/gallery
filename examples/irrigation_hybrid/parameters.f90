Subroutine parameters(npar, par, T, k1, k2, Ss, Sw, Sh, Fmax, Qbar, alpha, r, Bmax, B0, eta, pzero)
    implicit none
    integer,            intent(in)  :: npar
    double precision,   intent(in)  :: par(npar)
    double precision,   intent(out) :: T, k1, k2, Ss, Sw, Sh, Fmax, Qbar, alpha, r, Bmax, B0, eta, pzero

    !local variables
    integer :: i

    i       =  1;
    eta     =  par(i)       ; i = i + 1;

    T       =  1d0
    k1      =  2.1d0
    k2      =  5.0d0
    Ss      =  0.7d0
    Sw      =  0.4d0
    Sh      =  0.2d0
    Fmax    =  1.2d0
    Qbar    =  0.1d0
    alpha   =  4.0d0
    r       =  25d0
    Bmax    =  1d0
    B0      =  0.0005d0
    pzero   =  -1d0

End Subroutine parameters