TAP=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8
#OPTT='-tangent -fixinterface -inputlanguage fortran90 -outputlanguage fortran90 -nooptim activity'
OPTT='-tangent -fixinterface -inputlanguage fortran90 -outputlanguage fortran90'

# H0
TAPENADE_HOME=${TAP} ${TAP}/bin/tapenade ${OPTT} -O ./ -tgtfuncname _dc0 -head "h0fun(x, p)>(h0)" \
-o h0fun h0fun.f90 parameters.f90

# H1
TAPENADE_HOME=${TAP} ${TAP}/bin/tapenade ${OPTT} -O ./ -tgtfuncname _dc1 -head "h1fun(p)>(h1)" \
-o h1fun h1fun.f90 parameters.f90

# H01
TAPENADE_HOME=${TAP} ${TAP}/bin/tapenade ${OPTT} -O ./ -tgtfuncname _d01 -head "h01fun(t, x, p, par)>(h01)" \
-o h01fun h01fun.f90 h1fun_dc1.f90 h0fun_dc0.f90 h1fun.f90 h0fun.f90 parameters.f90