import numpy as np
import scipy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time
from nutopy import nle
from nutopy import tools
from nutopy import ocp
from scipy.linalg import null_space
from gve import *
from os import system

# Definition of optical parameters 
rho   = 0.6         # Specular reflection coefficient
a     = 1. - rho    # Absorption coefficient
s     = 0.94        # Diffuse reflection coefficient 
eps_f = 0.05        # Emissivity front coeff 0.05
eps_b = 0.55        # Emissivity back coeff 0.55
Bf    = 0.79        # Front Lambertian coeff 0.79
Bb    = 0.55        # Back Lambertian coeff 0.55
eps   = (Bf * eps_f - Bb * eps_b) / (eps_b + eps_f) 

# Initial guess of p0 taken from Matlab Convex Programming
pI         = np.array([ - 0.1577,  -0.3003, -0.1011,  -0.2680, 1.0000])
M1_guess   = 1.2264
M2_guess   = 5.0001
I1         = 10**(-8) * np.array([-0.9624, 0.1886, 0.6944, -0.0214, 0.0558])
I2         = 10**(-8) * np.array([-0.9633, 0.1899, 0.6947, -0.0220, 0.0559])

def deg2rad(x):
    y = x * np.pi / 180.
    return y


# Initial state
cont    = 0.                                                                                 # 0 for triangular cone, 1 for real drop 
mu      = 1.
I       = np.array([deg2rad(10), deg2rad(20), deg2rad(30), 1., 0.5])                         # Initial state
d       = np.array([[ 0.,  0.,  0.,  0.,  1.]])                                              # Direction of the displacement
b       = np.array([1. - rho * s, 2. * rho * s, Bf * rho * (1. - s) + (1. - rho) * eps ])
I0      = np.array([0., 0., 0., 0., 0])                                                      # delta I (0)
M0      = 0

# Real cone force definition
cBeta 	= (- b[0] * b[2]  - 2. * b[1] * b[2] + \
             np.sqrt(b[0] ** 2 * b[2] ** 2 - 4. * b[0] * b[2] ** 2 * b[1] + 8. * b[0] ** 2 * b[1] ** 2 + 4. * b[1] ** 3 * b[0])) / \
             (4. * b[0] * b[1] + 2. * b[1] ** 2);
sBeta 	= np.sqrt(1 - cBeta ** 2);
fs    	= b[0] * cBeta + (b[1] * cBeta ** 2 + b[2] * cBeta) * cBeta;
fperp 	= (b[1] * cBeta ** 2 + b[2] * cBeta) * sBeta;
fCone 	= np.array([fs, fperp])


pars    = np.hstack((mu, I, fCone, b))
d_orth  = scipy.linalg.null_space(d)                                                                      # Orthogonal space of b
d_orth  = d_orth.transpose()

# Display hfun code
# system('pygmentize hfun.f90')

# Compilation of the hamiltonian and associated derivatives
system('python -m numpy.f2py -c hfun.f90     hfun.pyf     -m hfun     > /dev/null 2>&1')
system('python -m numpy.f2py -c hfun_d.f90   hfun_d.pyf   -m hfun_d   > /dev/null 2>&1')
system('python -m numpy.f2py -c hfun_d_d.f90 hfun_d_d.pyf -m hfun_d_d > /dev/null 2>&1')

from hfun     import hfun     as hf
from hfun_d   import hfun_d   as hf_d
from hfun_d_d import hfun_d_d as hf_d_d

hfun_u   = lambda M, q, p, cont                   : hf.hfun_u(M, q, p, pars, cont)
dhfun_u  = lambda M, q, dq, p, dp, cont           : hf_d.hfun_u_d(M, q, dq, p, dp, pars, cont)
d2hfun_u = lambda M, q, dq, d2q, p, dp, d2p, cont : hf_d_d.hfun_u_d_d(M, q, d2q, dq, p, dp, d2p, pars, cont)

def dhfun_0(t, q, dq, p, dp):
    hd = 0.0
    return hd

def d2hfun_0(t, q, dq, d2q, p, dp, d2p):
    hdd = 0.0
    return hdd

def hfun_0(t, q, p):
    h = 0.0
    return h

hfun_u      = tools.tensorize(dhfun_u, d2hfun_u, tvars=(2, 3), full=True)(hfun_u)
hfun_0      = tools.tensorize(dhfun_0, d2hfun_0, tvars=(2, 3), full=True)(hfun_0)
Hu          = ocp.Hamiltonian(hfun_u)
H0          = ocp.Hamiltonian(hfun_0)
fu          = ocp.Flow(Hu)
f0          = ocp.Flow(H0)

def dshoot(z, dz, pars, d, d_orth, cont):
    I0  = np.zeros(5)
    M0  = 0.
    Mf  = 2. * np.pi

    pI  = z[0 : 5]
    M1  = z[5]
    M2  = z[6]
    I1  = z[7 : 12]
    I2  = z[12 : 17]

    dpI = dz[0 : 5]
    dM1 = dz[5]
    dM2 = dz[6]
    dI1 = dz[7 : 12]
    dI2 = dz[12 : 17]

    #----------------------------------
    (I1sol, dI1sol), (pI1, dpI1) = fu(M0, I0, (pI, dpI), (M1, dM1), pars, cont)
    (I2sol, dI2sol), (pI2, dpI2) = f0((M1, dM1), (I1, dI1), (pI, dpI), (M2, dM2), pars, cont)
    (If, dIf), (pIf, dpIf)       = fu((M2, dM2), (I2, dI2), (pI, dpI), Mf, pars, cont)

    #----------------------------------
    s           = np.zeros(17)
    s[0]        = pIf[0] * d[0] + pIf[1] * d[1] + pIf[2] * d[2] + pIf[3] * d[3] + pIf[4] * d[4]
    s[1 : 5]    = d_orth * If.transpose()

    s[7 : 12]   = I1 - I1sol 
    s[12 : 17]  = I2 - I2sol

    ds          = np.zeros(17)
    ds[0]       = dpIf[0] * d[0] + dpIf[1] * d[1] + dpIf[2] * d[2] + dpIf[3] * d[3] + dpIf[4] * d[4]
    ds[1 : 5]   = d_orth * dIf.transpose()

    ds[7 : 12]  = dI1 - dI1sol
    ds[12 : 17] = dI2 - dI2sol

    s[5], ds[5] = Hu((M1, dM1), (I1, dI1), (pI, dpI), pars, cont) #- H0((M1, dM1), (I1, dI1), (pI, dpI), pars, cont)
    s[6], ds[6] = Hu((M2, dM2), (I2, dI2), (pI, dpI), pars, cont) #- H0((M2, dM2), (I2, dI2), (pI, dpI), pars, cont)

    # on impose pas la continuité de pI car il est constant
    # il faut imposer la continuité de l'etat deltaI entre les switchs

    if not next: return s
    else: return s, (Mf, If, pIf, None)

def shoot(z, pars, d, d_orth, cont):
    I0 = np.zeros(5)
    M0 = 0.
    Mf = 2. * np.pi 
    pI = z[0 : 5]
    M1 = z[5]
    M2 = z[6]
    I1 = z[7 : 12]
    I2 = z[12 : 17]

    #----------------------------------
    I1sol, pI1 = fu(M0, I0, pI, M1)
    I2sol, pI2 = f0(M1, I1, pI, M2)
    If, pIf    = fu(M2, I2, pI, Mf)

    #----------------------------------
    s        = np.zeros(17)
    s[0]     = pIf[0] * d[0] + pIf[1] * d[1] + pIf[2] * d[2] + pIf[3] * d[3] + pIf[4] * d[4]
    s[1 : 5] = d_orth * If.transpose()

    # theta1            = atan2(pI1*sPerp/(pI1*sDir))
    # theta2            = atan2(pI2*sPerp/(pI2*sDir))
    # s[5]                      = theta1 - cone_alpha - deg2rad(90.)
    # s[6]                      = theta2 - cone_alpha - deg2rad(90.)

    s[5]        = Hu(M1, I1, pI, pars, cont)
    s[6]        = Hu(M2, I2, pI, pars, cont)
    s[7 : 12]   = I1 - I1sol 
    s[12 : 17]  = I2 - I2sol

    # on impose pas la continuité de pI car il est constant
    # il faut imposer la continuité de l'etat deltaI entre les switchs

    if not next: return s
    else: return s, (Mf, If, pIf, None)



