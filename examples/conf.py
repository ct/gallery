#!/usr/bin/env python3

import os
import sys

# Use sphinx-quickstart to create your own conf.py file!
# After that, you have to edit a few things.  See below.

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
# Select nbsphinx and, if needed, other Sphinx extensions:
extensions = [
    'nbsphinx',
    'nbsphinx_link',                # A sphinx extension for including notebook files from outside the sphinx source root.
    'sphinx_copybutton',            # for "copy to clipboard" buttons
    'sphinx.ext.mathjax',           # for math equations
    'sphinx.ext.viewcode',
#    'sphinxcontrib.bibtex',         # for bibliographic references
    'sphinx_gallery.load_style',    # load CSS for gallery (needs SG >= 0.6)
]

# images for the gallery
nbsphinx_thumbnails = {
    'homotopy-julia/FGS': 'homotopy-julia/thumbnail.png',
    'callback-simple/singular1d': 'callback-simple/cb.png',
    'irrigation_hybrid/hybrid': 'irrigation_hybrid/thumbnail.png',
    'irrigation/irrigation': 'irrigation/image.jpg',
    'irrigation_and_rainfall/irrigation_and_rainfall': 'irrigation_and_rainfall/image.jpg',
    'solarsail/solarsail-simple-version': 'solarsail/solarsail.jpg',
    'solarsail/solarsail-simple-version-implicit': 'solarsail/solarsail.jpg',
    'substrate/depletion': 'substrate/depletion.png',
    'bacteria/bacteria': 'bacteria/maintenance.png',
    'stability/stability': 'stability/stability.jpg',
    'bistable/bistable': 'bistable/thumbnail.png',
    'clr/clr': 'clr/thumbnail.png',
    'nav/nav-julia-gen': 'nav/thumbnail.png',
    'regulator/regulator': 'regulator/regulator.png',
    'simple_shooting/simple_shooting': 'simple_shooting/simple_shooting.png',
    'shooting_tutorials/simple_shooting_general': 'shooting_tutorials/simple_shooting.png',
    'shooting_tutorials/multiple_shooting_homotopy': 'shooting_tutorials/multiple_shooting_homotopy.png',
    'ihm/ihm': 'ihm/bocop-swimmer.png',
    'nlp_indirect/nlp1': 'nlp_indirect/nlp1.png',
    'nlp_indirect/nlp2': 'nlp_indirect/nlp2.png',
    'kepler/kepler': 'kepler/kepler.jpg',
    'kepler-c/kepler': 'kepler-c/kepler.jpg',
    'kepler-numba/kepler': 'kepler-numba/kepler.jpg',
    'kepler-j/kepler': 'kepler-j/kepler.jpg',
    'goddard/goddard': 'goddard/goddard.jpg',
    'goddard-j/goddard': 'goddard-j/goddard.jpg',
    'goddard-j/goddard_InfOpt': 'goddard-j/goddard.jpg',
    'geometry2D/ellipsoid/ellipsoid': 'geometry2D/ellipsoid/ellipsoid.png',
    'geometry2D/skeleton/skeleton': 'geometry2D/skeleton/skeleton.png',
    'turnpike/1d/bsb_turnpike': 'turnpike/1d/bsb_turnpike_1d.png',
    'turnpike/2d/turnpike': 'turnpike/2d/turnpike_2d.png',
    'control-loss/double-integrator-with-control-loss-and-no-control': 'control-loss/control-loss.jpg',
}

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
# Exclude build directory and Jupyter backup files:
exclude_patterns = ['_build', '**.ipynb_checkpoints', 'Thumbs.db', '.DS_Store']

# List of arguments to be passed to the kernel that executes the notebooks:
# If you use Matplotlib for plots, this setting is recommended:
nbsphinx_execute_arguments = [
    "--InlineBackend.figure_formats={'svg', 'pdf'}",
    "--InlineBackend.rc={'figure.dpi': 96}",
]

nbsphinx_execute = 'never'

# To get a prompt similar to the Classic Notebook, use
#nbsphinx_input_prompt = ' In [%s]:'
#nbsphinx_output_prompt = ' Out [%s]:'
#nbsphinx_prompt_width = '10'

# This is processed by Jinja2 and inserted before each notebook
nbsphinx_prolog = r"""
{% set docname = 'examples/' + env.doc2path(env.docname, base=None) %}

.. raw:: html

    <div class="admonition note">
      <p>Notebook source code:
        <a class="reference external" href="https://gitlab.inria.fr/ct/gallery/-/blob/master/{{ docname|e }}">{{ docname|e }}</a>
        <br>Run the notebook yourself on binder
        <a href="https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fct%2Fgallery-env.git/master?urlpath=lab/tree/{{
        docname|e }}"><img alt="Binder badge" src="https://static.mybinder.org/badge_logo.svg" style="vertical-align:text-bottom"></a>
      </p>
    </div>

.. raw:: latex

    \nbsphinxstartnotebook{\scriptsize\noindent\strut
    \textcolor{gray}{The following section was generated from
    \sphinxcode{\sphinxupquote{\strut {{ docname | escape_latex }}}} \dotfill}}
"""

MathJax = {
    'TeX': {'equationNumbers': {'autoNumber': 'AMS', 'useLabelIds': True}},
}

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
# -- The settings below this line are not specific to nbsphinx ------------
#

# The suffix(es) of source filenames.
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown'
}

#---sphinx-themes-----
html_theme = 'pydata_sphinx_theme'

html_theme_options = {
#    "collapse_navigation": True,
    "navigation_depth": 1,
    "show_toc_level": 1,
    "use_edit_page_button": False,
    "external_links": [
        {"url": "https://ct.gitlabpages.inria.fr/bocop3/", "name": "Bocop Docs"},
        {"url": "https://ct.gitlabpages.inria.fr/nutopy/", "name": "Nutopy Docs"}
    ],
    "gitlab_url": "https://gitlab.inria.fr/ct/gallery",
     "navbar_align": "left",  # [left, content, right] For testing that the navbar items align properly
     "navbar_end": ["search-field", "navbar-icon-links"],  # Just for testing
     "search_bar_text": "Search...",
     #"page_sidebar_items": ["page-toc", "edit-this-page"],
     #"footer_items": ["copyright", "sphinx-version", ""]
}


# Remove the sidebar from some pages
html_sidebars = {
    "**": [], # no left sidebar
#    "**": ["sidebar-nav-bs", "sidebar-ethical-ads"],
}

html_context = {
    "gitlab_url": "https://gitlab.inria.fr", # or your self-hosted GitLab
    "gitlab_user": "ct",
    "gitlab_repo": "gallery",
    "gitlab_version": "master",
    "doc_path": "examples",
}

html_logo   = 'logo-ct.svg'

author      = 'Olivier Cots'
project     = u'control toolbox gallery'
import time
copyright   =  u'%s, ct gallery project' % time.strftime('%Y') # a changer
#version     = '0.2' # a voir comment gerer doc et version
#release     = '0.2.1'

master_doc = 'index'


# -- Get version information and date from Git ----------------------------

try:
    from subprocess import check_output
    release = check_output(['git', 'describe', '--tags', '--always'])
    release = release.decode().strip()
    today = check_output(['git', 'show', '-s', '--format=%ad', '--date=short'])
    today = today.decode().strip()
except Exception:
    release = '<unknown>'
    today = '<unknown date>'

# -- Options for HTML output ----------------------------------------------

html_title = "ct gallery" #+ ' version ' + release
