// +++DRAFT+++ This class implements the OCP functions
// It derives from the generic class bocop3OCPBase
// OCP functions are defined with templates since they will be called
// from both the NLP solver (double arguments) and AD tool (ad_double arguments)
//#pragma once

#include <OCP.h>
// ///////////////////////////////////////////////////////////////////


template <typename Variable>
void OCP::finalCost(double initial_time, double final_time, const Variable *initial_state, const Variable *final_state, const Variable *parameters, const double *constants, Variable &final_cost)
{
  // minimise time
  final_cost = parameters[0];
}


template <typename Variable>
void OCP::dynamics(double time, const Variable *state, const Variable *control, const Variable *parameters, const double *constants, Variable *state_dynamics)
{
  double w = constants[0];
  double l = constants[1];

  Variable x = state[0];
  Variable y = state[1];
  Variable theta = state[2];
  Variable beta1 = state[3];
  Variable beta2 = state[4];
  Variable beta3 = state[5];

  state_dynamics[0] = w + cos(theta);
  state_dynamics[1] = sin(theta);
  state_dynamics[2] = control[0];
  state_dynamics[3] = -control[0] - sin(beta1) / l;
  state_dynamics[4] = -control[0] - cos(beta1) * sin(beta2 - beta1) / l;
  state_dynamics[5] = -control[0] - cos(beta1) * cos(beta2 - beta1) * sin(beta3 - beta2) / l;

  // free final time: rescale dynamics
  Variable tf = parameters[0];
    
  for (size_t i=0; i<6; i++)
    state_dynamics[i] *= tf;
}

template <typename Variable>
void OCP::boundaryConditions(double initial_time, double final_time, const Variable *initial_state, const Variable *final_state, const Variable *parameters, const double *constants, Variable *boundary_conditions)
{
  // INITIAL CONDITIONS FOR NAVIGATION PROBLEM
  boundary_conditions[0] = initial_state[0]; //x0=0
  boundary_conditions[1] = initial_state[1]; //y0=0
  boundary_conditions[2] = initial_state[2]; //theta=pi/7
  boundary_conditions[3] = initial_state[3]; //beta1=0
  boundary_conditions[4] = initial_state[4]; //beta2=0
  boundary_conditions[5] = initial_state[5]; //beta3=0

  // FINAL CONDITIONS FOR NAVIGATION PROBLEM
  boundary_conditions[6] = final_state[0]; //xf=4
  boundary_conditions[7] = final_state[1]; //yf=7
  boundary_conditions[8] = final_state[2]; //theta_f=pi/2
  boundary_conditions[9] = final_state[3]; //beta1_f=0
  boundary_conditions[10] = final_state[4]; //beta2_f=0
  boundary_conditions[11] = final_state[5]; //beta3_f=0
}

template <typename Variable>
void OCP::pathConstraints(double time, const Variable *state, const Variable *control, const Variable *parameters, const double *constants, Variable *path_constraints)
{}

void OCP::preProcessing()
{}

// ///////////////////////////////////////////////////////////////////
// explicit template instanciation for template functions, with double and double_ad 
// +++ could be in an included separate file ? 
// but needs to be done for aux functions too ? APPARENTLY NOT !
template void OCP::finalCost<double>(double initial_time, double final_time, const double *initial_state, const double *final_state, const double *parameters, const double *constants, double &final_cost);
template void OCP::dynamics<double>(double time, const double *state, const double *control, const double *parameters, const double *constants, double *state_dynamics);
template void OCP::boundaryConditions<double>(double initial_time, double final_time, const double *initial_state, const double *final_state, const double *parameters, const double *constants, double *boundary_conditions);
template void OCP::pathConstraints<double>(double time, const double *state, const double *control, const double *parameters, const double *constants, double *path_constraints);

template void OCP::finalCost<double_ad>(double initial_time, double final_time, const double_ad *initial_state, const double_ad *final_state, const double_ad *parameters, const double *constants, double_ad &final_cost);
template void OCP::dynamics<double_ad>(double time, const double_ad *state, const double_ad *control, const double_ad *parameters, const double *constants, double_ad *state_dynamics);
template void OCP::boundaryConditions<double_ad>(double initial_time, double final_time, const double_ad *initial_state, const double_ad *final_state, const double_ad *parameters, const double *constants, double_ad *boundary_conditions);
template void OCP::pathConstraints<double_ad>(double time, const double_ad *state, const double_ad *control, const double_ad *parameters, const double *constants, double_ad *path_constraints);
