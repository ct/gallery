subroutine hfun(x, p, e, h)

    double precision, intent(in)  :: x(1), p(1), e
    double precision, intent(out) :: h

    double precision :: u
    
    if(abs(p(1))<1e-8)then
        u = 0d0
    else
        u = (-e + sqrt(e**2+p(1)**2))/p(1)
    end if
        
    h = p(1)*u - x(1)**2 + e*(log(1d0-u**2))
    
end subroutine hfun
