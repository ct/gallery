# hfun -> hfun_d
TAPENADE_HOME=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8 \
/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8/bin/tapenade -tangent -fixinterface \
-inputlanguage fortran90 -outputlanguage fortran90 -O ./ -tgtfuncname _d -head "hfun(x, p, e)>(h)" \
-o hfun \
hfun.f90

# hfun_d -> hfun_d_d
TAPENADE_HOME=/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8 \
/Users/ocots/Boulot/recherche/Logiciels/dev/hampath/code/src/tapenade3.8/bin/tapenade -tangent -fixinterface \
-inputlanguage fortran90 -outputlanguage fortran90 -O ./ -tgtfuncname _d -head "hfun_d(x, p, e)>(hd)" \
-o hfun_d \
hfun_d.f90