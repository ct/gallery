using ForwardDiff, OrdinaryDiffEq

function Flow(h)

    function rhs!(dz, z, args, t)
        n = size(z, 1)÷2
        foo = z -> h(t, z[1:n], z[n+1:2n], args...)
        dh = ForwardDiff.gradient(foo, z)
        dz[1:n] = dh[n+1:2n]
        dz[n+1:2n] = -dh[1:n]
    end
    
    function f(tspan::Tuple{Number, Number}, x0, p0, 
            args...; abstol=1e-12, reltol=1e-12, saveat=[])
        z0 = [ x0 ; p0 ]
        ode = ODEProblem(rhs!, z0, tspan, args)
        sol = solve(ode, Tsit5(), abstol=abstol, reltol=reltol, saveat=saveat)
        return sol
    end
    
    function f(t0::Number, x0, p0, tf::Number, args...; abstol=1e-12, reltol=1e-12, saveat=[])
        sol = f((t0, tf), x0, p0, args..., abstol=abstol, reltol=reltol, saveat=saveat)
        n = size(x0, 1)
        return sol[1:n, end], sol[n+1:2*n, end]
    end
    
    return f

end
