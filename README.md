# Welcome to ct (control toolbox) gallery

Examples from the ct (control toolbox) project
using [bocop](https://ct.gitlabpages.inria.fr/bocop3), [nutopy](https://ct.gitlabpages.inria.fr/nutopy) and other packages. Please visit the [gallery](https://ct.gitlabpages.inria.fr/gallery) or explore the dashboards apps or the notebooks codes by clicking on the following images!

[<img src="examples/notebook-logo.png" alt="Visit the notebooks" width="270"/>](https://ct.gitlabpages.inria.fr/gallery/notebooks.html)
[<img src="examples/dashboard-logo.png" alt="Visit the dashboards" width="300"/>](https://ct.gitlabpages.inria.fr/gallery/dashboards.html)

You can execute the code online and make your own experiments by clicking on <img alt="Binder badge" src="https://static.mybinder.org/badge_logo.svg" style="vertical-align:text-bottom"> when you find it on a notebook or a dashboard.

Special thanks: [A. Hurtig](http://www.alain.les-hurtig.org) for the logos ligatures
